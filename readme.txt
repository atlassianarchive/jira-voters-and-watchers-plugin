JIRA Voters and Watchers Plugin
-----------------------------------------------------------------------

This plugin will allow you to create new custom fields for voters and watchers 
which will display the list of voters and watchers in the issue and in the issue 
navigator. It will also allow you to search for all the issues voted or watched 
by a particular user.

There is another webwork action in the plugin as well that should allow you to 
request and RSS feed of your votes -- unfortunately, there's a problem with it
and it doesn't work yet.
  



