jQuery(function($) {
    var voters = {
        getVoteFieldset : function(voteLinkContainer) {
            return voteLinkContainer.children("fieldset");
        },

        getVoteParams : function(voteLinkContainer) {
            var params = {};
            this.getVoteFieldset(voteLinkContainer).find("input").each(function() {
                params[this.name] = this.value;
            });

            return params;
        },

        getContextPath : function() {
            return contextPath; /* Retrieved from global var */
        },

        initVoteLink : function(voteLinkContainer) {
            voteLinkContainer.children("a").click(function() {
                var voteLink = $(this);
                var params = voters.getVoteParams(voteLinkContainer);
                var ajaxParams = {
                    type : "POST",
                    url : voters.getContextPath() + "/secure/VoteAction.jspa",
                    success : function(data) {
                        voteLinkContainer.html(data);
                        voters.initVoteLink(voteLinkContainer);
                    }
                };

                ajaxParams["data"] = {
                    id : params["issueId"],
                    vote : voteLink.hasClass("plugin_votersandwatchers_unvote_link") ? "unvote" : "vote",
                    decorator: "none"
                };

                $.ajax(ajaxParams);
                return false;
            });
        }
    };

    $("div.plugin_votersandwatchers_vote_link_container").each(function() {
        voters.initVoteLink($(this));
    });
});