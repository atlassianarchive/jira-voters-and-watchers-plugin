package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Lists;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class VotersCFType extends CalculatedCFType
{

    private final UserConverter userConverter;
    private final VoteManager voteManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    public VotersCFType(UserConverter userConverter, VoteManager voteManager, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.userConverter = userConverter;
        this.voteManager = voteManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public String getStringFromSingularObject(Object o)
    {
        return userConverter.getString((User) o);
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException
    {
        return userConverter.getUser(s);
    }

    public Object getValueFromIssue(CustomField customField, Issue issue)
    {
        if (voteManager.isVotingEnabled())
        {
            Collection<User> voters = voteManager.getVoters(issue, jiraAuthenticationContext.getLocale());
            List<User> l = new ArrayList<User>(voters);
            Collections.sort(l, new UserNameComparator());
            return l;
        }
        else
        {
            return "Voting not enabled.";
        }
    }

    public List<FieldIndexer> getRelatedIndexers(final CustomField customField)
    {
        FieldIndexer fieldIndexer = new FieldIndexer()
        {
            public String getId()
            {
                return customField.getId();
            }

            public void addIndex(Document doc, Issue issue)
            {
                @SuppressWarnings("unchecked")
                List<User> users = (List<User>) customField.getValue(issue);
                if (users != null)
                {
                    for (User user : users)
                    {
                        doc.add(
                                new Field(
                                        getDocumentFieldId(),
                                        userConverter.getString(user),
                                        isFieldVisibleAndInScope(issue) ? Field.Store.YES : Field.Store.NO, 
                                        Field.Index.NOT_ANALYZED
                                )
                        );
                    }
                }
            }

            public String getDocumentFieldId()
            {
                return getId();
            }

            public boolean isFieldVisibleAndInScope(Issue issue)
            {
                return voteManager.isVotingEnabled();
            }
        };
        return Lists.newArrayList(fieldIndexer);
    }

}




