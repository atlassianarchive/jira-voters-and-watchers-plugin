package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.avatar.Avatar;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.pager.PagerManager;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.issue.ViewIssue;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.util.profiling.UtilTimerStack;
import org.ofbiz.core.entity.GenericValue;

/**
 * Created by IntelliJ IDEA.
 * User: nolen
 * Date: Aug 7, 2006
 * Time: 3:50:39 PM
 * To change this template use File | Settings | File Templates.
 */
///CLOVER:OFF
// I can't instantiate this class for testing without getting a 'database' locked error. Fortunately, we have
// func tests as replacement
public class ViewIssueAlias extends ViewIssue
{
    private String vote;
    private String watch;
    private final VoteManager voteManager;
    private final WatcherManager watcherManager;
    private final PagerManager pagerManager;
    private final AvatarService avatarService;
    private final UserPickerSearchService userPickerSearchService;
    private final JiraWebResourceManager jiraWebResourceManager;

    public ViewIssueAlias(SubTaskManager subTaskManager, PluginAccessor pluginAccessor, FieldManager fieldManager, FieldScreenRendererFactory fieldScreenRendererFactory, FieldLayoutManager fieldLayoutManager, RendererManager rendererManager, CommentManager commentManager, ProjectRoleManager projectRoleManager, CommentService commentService, SimpleLinkManager simpleLinkManager, WebInterfaceManager webInterfaceManager, PermissionManager permissionManager, ModuleWebComponent moduleWebComponent, UserUtil userUtil, FeatureManager featureManager, VoteManager voteManager, WatcherManager watcherManager, AvatarService avatarService, EventPublisher eventPublisher, ApplicationProperties applicationProperties)
    {
        super(subTaskManager, pluginAccessor, fieldManager, fieldScreenRendererFactory, fieldLayoutManager, rendererManager, commentManager, projectRoleManager, commentService, ComponentAccessor.getComponentOfType(PagerManager.class), ComponentAccessor.getWebResourceManager(), simpleLinkManager, webInterfaceManager, permissionManager, moduleWebComponent, userUtil, featureManager, avatarService, eventPublisher, applicationProperties, ComponentAccessor.getComponentOfType(UserPickerSearchService.class));
        this.voteManager = voteManager;
        this.watcherManager = watcherManager;
        this.pagerManager = ComponentAccessor.getComponentOfType(PagerManager.class);
        this.avatarService = avatarService;
        this.userPickerSearchService = ComponentAccessor.getComponentOfType(UserPickerSearchService.class);
        this.jiraWebResourceManager = (JiraWebResourceManager) ComponentAccessor.getWebResourceManager();
    }

    protected String doExecute() throws Exception
    {
        try
        {
            GenericValue issue = getIssue();

            // VOTE-43 VOTE-44
            // Copied from the super class constructor which fixes both issue & search results sharing problem
            String issueKey = issue.getString("key");
            jiraWebResourceManager.putMetadata("can-edit-watchers", Boolean.toString(permissionManager.hasPermission(Permissions.MANAGE_WATCHER_LIST, getIssueObject(), getLoggedInUser())));
            jiraWebResourceManager.putMetadata("can-search-users", Boolean.toString(userPickerSearchService.canPerformAjaxSearch(getLoggedInUser())));
            jiraWebResourceManager.putMetadata("issue-key", issueKey);
            String defaultAvatarUrl = avatarService.getAvatarURL(getLoggedInUser(), null, Avatar.Size.SMALL).toString();
            jiraWebResourceManager.putMetadata("default-avatar-url", defaultAvatarUrl);

            UtilTimerStack.push("Updating Pager for viewing issue:" + issueKey);
            pagerManager.updatePager(getNextPreviousPager(), getSearchRequest(), getLoggedInUser(), issueKey);
            UtilTimerStack.pop("Updating Pager for viewing issue:" + issueKey);

            if (issue != null)
            {
                if (vote != null && voteManager.isVotingEnabled())
                {
                    if (vote.equalsIgnoreCase("vote"))
                    {
                        // VOTE-26
                        if(getLoggedInUser() != null && !getLoggedInUser().getName().equals(issue.getString(IssueFieldConstants.REPORTER)))
                            voteManager.addVote(getLoggedInUser(), issue);
                    }
                    else if (vote.equalsIgnoreCase("unvote"))
                    {
                        voteManager.removeVote(getLoggedInUser(), issue);
                    }
                    ComponentAccessor.getIssueIndexManager().reIndex(issue);

                    // if we are updating an issue, we should redirect
                    return getRedirect("/browse/" + getIssueObject().getKey());
                }

                if (watch != null && getApplicationProperties().getOption(APKeys.JIRA_OPTION_WATCHING))
                {
                    if (watch.equalsIgnoreCase("true"))
                    {
                        watcherManager.startWatching(getLoggedInUser(), issue);
                    }
                    else if (watch.equalsIgnoreCase("false"))
                    {
                        watcherManager.stopWatching(getLoggedInUser(), issue);
                    }

                    ComponentAccessor.getIssueIndexManager().reIndex(issue);

                    // if we are updating an issue, we should redirect
                    return getRedirect("/browse/" + getIssueObject().getKey());
                }

                //set the selected project to be current project
                if (getProject() != null)
                {
                    setSelectedProjectId(getProject().getLong("id"));
                }

                if (issue != null)
                {
                    pagerManager.updatePager(
                            pagerManager.getPager(),
                            getSearchRequest(),
                            getLoggedInUser(),
                            issue.getString("key")
                    );
                }
            }

            // VOTE-43 VOTE-44
            // Copied from the super class constructor which fixes both issue & search results sharing problem
            // This part fixes the UI while viewing an issue
            if (!useKickAss())
            {
                jiraWebResourceManager.requireResource("jira.webresources:viewissue");
            }
            jiraWebResourceManager.requireResource("jira.webresources:jira-fields");
            if (useKickAss())
            {
                jiraWebResourceManager.requireResource("com.atlassian.jira.jira-issue-nav-plugin:standalone-issue");
            }

            return SUCCESS;
        }
        catch (IssueNotFoundException ex)
        {
            addErrorMessage(getText("admin.errors.issues.issue.does.not.exist"));
            return ISSUE_NOT_FOUND_RESULT;
        }
        catch (IssuePermissionException ex)
        {
            addErrorMessage(getText("admin.errors.issues.no.browse.permission"));
            return PERMISSION_VIOLATION_RESULT;
        }
    }


    public String getVote()
    {
        return vote;
    }

    public void setVote(String vote)
    {
        this.vote = vote;
    }

    public String getWatch()
    {
        return watch;
    }

    public void setWatch(String watch)
    {
        this.watch = watch;
    }
}
///CLOVER:ON
