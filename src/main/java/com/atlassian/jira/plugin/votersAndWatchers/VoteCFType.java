package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.plugin.webresource.JiraWebResourceManager;
import com.atlassian.jira.security.JiraAuthenticationContext;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VoteCFType extends CalculatedCFType
{
    private static final UserNameComparator USER_NAME_COMPARATOR = new UserNameComparator();

    private final UserConverter userConverter;
    private final VoteManager voteManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final JiraWebResourceManager jiraWebResourceManager;

    public VoteCFType(UserConverter userConverter, VoteManager voteManager, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.userConverter = userConverter;
        this.voteManager = voteManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.jiraWebResourceManager = (JiraWebResourceManager) ComponentAccessor.getWebResourceManager();
    }

    public String getStringFromSingularObject(Object o)
    {
        return userConverter.getString((User) o);
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException
    {
        return userConverter.getUser(s);
    }

    /**
     * VOTE-62 - Return type of this method need to match in CalculatedCFType <T, S>
     *         - Else we'll get ClassCastException in getStringFromSingularObject method above
     */
    public Object getValueFromIssue(CustomField field, Issue issue)
    {
        if (voteManager.isVotingEnabled())
        {
            List<User> voters = voteManager.getVoters(issue, jiraAuthenticationContext.getLocale());
            Collections.sort(voters, USER_NAME_COMPARATOR);
            return voters;
        }

        return Collections.emptyList();
    }
    
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem)
    {
        jiraWebResourceManager.requireResource("com.atlassian.jira.plugin.votersAndWatchers:votecftype-resources");

        Map<String, Object> params = new HashMap<String, Object>();
        params.putAll(super.getVelocityParameters(issue, field, fieldLayoutItem));

        if (isVotedAlready(issue))
        {
            params.put("voted", Boolean.TRUE);
        } else
        {
            params.put("voted", Boolean.FALSE);
        }

        if (isIssueReportedByMe(issue))
        {
            params.put("reporter", Boolean.TRUE);
        } else
        {
            params.put("reporter", Boolean.FALSE);
        }

        if (issue.getResolution() == null)
        {
            params.put("resolved", Boolean.FALSE);
        } else
        {
            params.put("resolved", Boolean.TRUE);

        }


        return params;
    }


    /**
     * Determine whether the current user has voted already or not
     */
    private boolean isVotedAlready(Issue issue)
    {
        boolean votedAlready = false;
        User user = jiraAuthenticationContext.getLoggedInUser();
        // VOTE-50 - NPE will happen when issue.getGenericValue is null
        if (user != null && issue.getGenericValue() != null)
        {
            votedAlready = voteManager.hasVoted(user, issue);
        }
        return votedAlready;
    }

    private boolean isIssueReportedByMe(Issue issue)
    {
        User reporter = issue.getReporter();
        User user = jiraAuthenticationContext.getLoggedInUser();
        return reporter != null && reporter.equals(user);

    }
}
