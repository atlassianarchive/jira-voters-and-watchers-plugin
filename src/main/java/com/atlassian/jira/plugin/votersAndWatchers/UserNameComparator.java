package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import org.apache.commons.lang.StringUtils;

import java.util.Comparator;

public class UserNameComparator implements Comparator<User>
{
    @Override
    public int compare(User left, User right)
    {
        int result = StringUtils.defaultString(left.getDisplayName()).compareTo(right.getDisplayName());

        return 0 == result ? StringUtils.defaultString(left.getName()).compareTo(right.getName()) : result;
    }
}
