package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.avatar.AvatarService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.RendererManager;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.pager.PagerManager;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.plugin.webfragment.SimpleLinkManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.web.action.issue.ViewIssue;
import com.atlassian.jira.web.component.ModuleWebComponent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.WebInterfaceManager;
import org.ofbiz.core.entity.GenericValue;

///CLOVER:OFF
// I can't instantiate this class for testing without getting a 'database' locked error. Fortunately, we have
// func tests as replacement
public class VoteAction extends ViewIssue
{
    private final VoteManager voteManager;
    private String vote;

    public VoteAction(SubTaskManager subTaskManager, PluginAccessor pluginAccessor, FieldManager fieldManager, FieldScreenRendererFactory fieldScreenRendererFactory, FieldLayoutManager fieldLayoutManager, RendererManager rendererManager, CommentManager commentManager, ProjectRoleManager projectRoleManager, CommentService commentService, SimpleLinkManager simpleLinkManager, WebInterfaceManager webInterfaceManager, PermissionManager permissionManager, ModuleWebComponent moduleWebComponent, UserUtil userUtil, FeatureManager featureManager, VoteManager voteManager, AvatarService avatarService, EventPublisher eventPublisher, ApplicationProperties applicationProperties)
    {
        super(subTaskManager, pluginAccessor, fieldManager, fieldScreenRendererFactory, fieldLayoutManager, rendererManager, commentManager, projectRoleManager, commentService, ComponentAccessor.getComponentOfType(PagerManager.class), ComponentAccessor.getWebResourceManager(), simpleLinkManager, webInterfaceManager, permissionManager, moduleWebComponent, userUtil, featureManager, avatarService, eventPublisher, applicationProperties, ComponentAccessor.getComponentOfType(UserPickerSearchService.class));
        this.voteManager = voteManager;
    }

    protected String doExecute() throws Exception
    {
        GenericValue issue = getIssue();
        if (issue != null)
        {

            if (vote != null && voteManager.isVotingEnabled())
            {
                if (vote.equalsIgnoreCase("vote"))
                {
                    voteManager.addVote(getLoggedInUser(), issue);
                }
                else if (vote.equalsIgnoreCase("unvote"))
                {
                    voteManager.removeVote(getLoggedInUser(), issue);
                }
            }
            ComponentAccessor.getIssueIndexManager().reIndex(issue);

        }

        return SUCCESS;
    }

    public String getVote()
    {
        return vote;
    }

    public void setVote(String vote)
    {
        this.vote = vote;
    }
}
///CLOVER:ON
