package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.customfields.impl.FieldValidationException;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.google.common.collect.Lists;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class WatchersCFType extends CalculatedCFType
{
    private final UserConverter userConverter;
    private final WatcherManager watcherManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final ApplicationProperties applicationProperties;

    public WatchersCFType(UserConverter userConverter, WatcherManager watcherManager, JiraAuthenticationContext jiraAuthenticationContext, ApplicationProperties applicationProperties)
    {
        this.userConverter = userConverter;
        this.watcherManager = watcherManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
        this.applicationProperties = applicationProperties;
    }

    public String getStringFromSingularObject(Object o)
    {
        return userConverter.getString((User) o);
    }

    public Object getSingularObjectFromString(String s) throws FieldValidationException
    {
        return userConverter.getUser(s);
    }

    public Object getValueFromIssue(CustomField customField, Issue issue)
    {
        Collection<User> watchers = watcherManager.getCurrentWatchList(issue, jiraAuthenticationContext.getLocale());
        List<User> l = new ArrayList<User>(watchers);
        Collections.sort(l, new UserNameComparator());
        
        return l;
    }

    public List<FieldIndexer> getRelatedIndexers(final CustomField customField)
    {
        FieldIndexer fieldIndexer = new FieldIndexer()
        {
            public String getId()
            {
                return customField.getId();
            }

            public void addIndex(Document doc, Issue issue)
            {
                @SuppressWarnings("unchecked")
                List<User> users = (List<User>) customField.getValue(issue);
                if (users != null)
                {
                    for (User user : users)
                        doc.add(
                                new Field(
                                        getDocumentFieldId(),
                                        userConverter.getString(user),
                                        isFieldVisibleAndInScope(issue) ? Field.Store.YES : Field.Store.NO, 
                                        Field.Index.NOT_ANALYZED
                                )
                        );
                }
            }

            public String getDocumentFieldId()
            {
                return getId();
            }

            public boolean isFieldVisibleAndInScope(Issue issue)
            {
                return applicationProperties.getOption("jira.option.watching");
            }
        };
        return Lists.newArrayList(fieldIndexer);
    }

}
