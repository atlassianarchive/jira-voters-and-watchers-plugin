package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.jira.bc.issue.watcher.WatcherService;
import com.atlassian.jira.bc.user.search.UserPickerSearchService;
import com.atlassian.jira.issue.index.IndexException;
import com.atlassian.jira.issue.index.IssueIndexManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.template.VelocityTemplatingEngine;
import com.atlassian.jira.web.action.issue.ManageWatchers;
import org.apache.log4j.Logger;
import org.ofbiz.core.entity.GenericEntityException;
import webwork.action.ActionSupport;

import java.util.Arrays;
import java.util.Collection;

/**
 * A subclass of {@link com.atlassian.jira.web.action.issue.ManageWatchers}
 * that reindexes the issue when watchers are added/removed.
 */
// This whole class will be integration tested.
///CLOVER:OFF
public class ManageWatcherAlias extends ManageWatchers
{
    private static final Logger logger = Logger.getLogger(ManageWatcherAlias.class);

    private static final Collection<String> WATCHER_ERROR_RESULTS = Arrays.asList(ActionSupport.ERROR, "securitybreach");

    private final IssueIndexManager issueIndexManager;

    public ManageWatcherAlias(WatcherManager watcherManager, VelocityTemplatingEngine velocityTemplatingEngine, UserPickerSearchService searchService, PermissionManager permissionManager, CrowdService crowdService, IssueIndexManager issueIndexManager)
    {
        super(watcherManager, velocityTemplatingEngine, searchService, permissionManager, crowdService);
        this.issueIndexManager = issueIndexManager;
    }

    private void checkReindexIssue(String result)
    {
        if (!WATCHER_ERROR_RESULTS.contains(result))
            try
            {
                issueIndexManager.reIndex(getIssueObject());
            }
            catch (IndexException ie)
            {
                logger.error("Unable to reindex issue " + getIssueObject().getKey(), ie);
            }
    }

    @Override
    public String doStartWatchers() throws GenericEntityException
    {
        String result = super.doStartWatchers();
        checkReindexIssue(result);

        return result;
    }

    @Override
    public String doStopWatchers() throws GenericEntityException
    {
        String result = super.doStopWatchers();
        checkReindexIssue(result);
        return result;
    }
}
///CLOVER:ON
