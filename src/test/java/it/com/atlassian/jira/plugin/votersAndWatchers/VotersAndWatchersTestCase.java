package it.com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.jira.functest.framework.FuncTestCase;
import com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertions;
import com.atlassian.jira.functest.framework.assertions.IssueNavigatorAssertionsImpl;
import com.atlassian.jira.functest.framework.navigation.IssueNavigation;
import com.atlassian.jira.functest.framework.navigation.IssueNavigationImpl;
import com.atlassian.jira.functest.framework.util.url.URLUtil;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.SimpleHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class VotersAndWatchersTestCase extends FuncTestCase
{
    private HttpClient httpClient;
    private IssueNavigation issueNavigation;
    private IssueNavigatorAssertions issueNavigatorAssertions;

    @Override
    public void setUpTest()
    {
        super.setUpTest();
        administration.restoreData("testdata.zip");
        httpClient = new HttpClient(new SimpleHttpConnectionManager());
        issueNavigation = new IssueNavigationImpl(tester, environmentData);
        issueNavigatorAssertions = new IssueNavigatorAssertionsImpl(tester, environmentData);
    }

    public void testSearchForIssuesUsingOneWatcherThatWatchesAllIssues()
    {
        // The test library that the test is using is outdated and no longer maintained.
        // So we have to use gotoPage to get the issue navigator to work. Same goes to all the test in this file.
        // https://jira.atlassian.com/browse/JRA-30704
        navigation.gotoPage("issues/?jql=\"Watchers Field\" = admin");

        /* Admin is watching two issues */
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "2", "2");
        tester.assertLinkPresentWithText("TST-1");
        tester.assertLinkPresentWithText("TST-2");
    }

    public void testSearchForIssuesUsingOneWatcherThatWatchesOnlyOneIssue()
    {
        navigation.gotoPage("issues/?jql=\"Watchers Field\" = user1");

        /* user1 watching TST-2 only */
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "1", "1");
        tester.assertLinkPresentWithText("TST-2");
    }

    public void testSearchForIssuesUsingOneVoterThatVotedForAllIssues()
    {
        navigation.gotoPage("issues/?jql=\"Voters Field\" = user2");

        /* user2 voted for two issues */
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "2", "2");
        tester.assertLinkPresentWithText("TST-1");
        tester.assertLinkPresentWithText("TST-2");
    }

    public void testSearchForIssuesUsingOneVoterThatVotedForOnlyOneIssue()
    {
        navigation.gotoPage("issues/?jql=\"Voters Field\" = user1");

        /* user1 voted for only one issue */
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "1", "1");
        tester.assertLinkPresentWithText("TST-2");
    }

    public void testVoteCannotBePlacedForOwnIssuesViaVoteCustomField()
    {
        navigation.issueNavigator().gotoNavigator();
        navigation.issueNavigator().addColumnToIssueNavigator(new String[]{"Vote Custom Field"});
        navigation.gotoPage("issues/?jql=summary ~ \"Admin Issue 01\"");

        tester.assertLinkPresentWithText("TST-2");
        tester.assertTextPresent("Can't vote on your own issues");
    }

    public void testWatcherFieldRendered()
    {
        issueNavigation.gotoIssue("TST-2");

        tester.assertTextPresent("Watchers Field:");
        tester.assertLinkPresentWithText("admin");
        tester.assertLinkPresentWithText("User1", 1);
        tester.assertLinkPresentWithText("User2", 1);
    }

    public void testVoteFieldRendered()
    {
        issueNavigation.gotoIssue("TST-2");

        tester.assertTextPresent("Voters Field:");
        tester.assertLinkPresentWithText("User1", 0);
        tester.assertLinkPresentWithText("User2", 0);
    }

    private int getPageWithHttpClient(String url, Map<String, Collection<String>> requestParams) throws IOException
    {
        GetMethod method = new GetMethod(tester.getTestContext().getBaseUrl() + url);
        Collection<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

        for (Map.Entry<String, Collection<String>> entry : requestParams.entrySet())
            for (String value : entry.getValue())
                nameValuePairs.add(new NameValuePair(entry.getKey(), value));

        method.setQueryString(nameValuePairs.toArray(new NameValuePair[nameValuePairs.size()]));

        try
        {
            return httpClient.executeMethod(method);
        }
        finally
        {
            method.releaseConnection();
        }
    }

    private void addVote(String issueId) throws IOException
    {
        Map<String, Collection<String>> requestParams = new HashMap<String, Collection<String>>();

        requestParams.put("id", Arrays.asList(issueId));
        requestParams.put("vote", Arrays.asList("vote"));
        requestParams.put("decorator", Arrays.asList("none"));
        requestParams.put("os_username", Arrays.asList(ADMIN_USERNAME));
        requestParams.put("os_password", Arrays.asList(ADMIN_PASSWORD));

        assertEquals(200, getPageWithHttpClient("/secure/VoteAction.jspa", requestParams));
    }

    private void removeVote(String issueId) throws IOException
    {
        Map<String, Collection<String>> requestParams = new HashMap<String, Collection<String>>();

        requestParams.put("id", Arrays.asList(issueId));
        requestParams.put("vote", Arrays.asList("unvote"));
        requestParams.put("decorator", Arrays.asList("none"));
        requestParams.put("os_username", Arrays.asList(ADMIN_USERNAME));
        requestParams.put("os_password", Arrays.asList(ADMIN_PASSWORD));

        assertEquals(200, getPageWithHttpClient("/secure/VoteAction.jspa", requestParams));
    }

    public void testIssueNotVotedCreatedByOtherUserIsVotable() throws IOException
    {
        navigation.issueNavigator().gotoNavigator();
        navigation.issueNavigator().addColumnToIssueNavigator(new String[]{"Vote Custom Field"});
        navigation.gotoPage("issues/?jql=summary ~ \"User 1 Issue 01\"");
        tester.assertLinkPresentWithText("Vote");

        addVote("10000");
        navigation.gotoPage("issues/?jql=summary ~ \"User 1 Issue 01\"");
        tester.assertLinkPresentWithText("Unvote");
    }

    public void testUnvoteIssue() throws IOException
    {
        navigation.issueNavigator().gotoNavigator();
        navigation.issueNavigator().addColumnToIssueNavigator(new String[]{"Vote Custom Field"});
        navigation.gotoPage("issues/?jql=summary ~ \"User 1 Issue 01\"");
        tester.assertLinkPresentWithText("Vote");

        addVote("10000");
        navigation.gotoPage("issues/?jql=summary ~ \"User 1 Issue 01\"");
        tester.assertLinkPresentWithText("Unvote");

        removeVote("10000");
        navigation.gotoPage("issues/?jql=summary ~ \"User 1 Issue 01\"");
        tester.assertLinkPresentWithText("Vote");
    }

    public void testRemovingWatchersViaWatcherManagerReindexesWatchersCustomField()
    {
        issueNavigation.gotoIssue("TST-2");
        tester.clickLinkWithText("User1", 1);
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "1", "1");

        issueNavigation.gotoIssue("TST-2");
        tester.clickLink("manage-watchers");
        tester.setWorkingForm("stopform");
        tester.checkCheckbox("stopwatch_user1", ".");
        tester.submit("remove");

        navigation.issueNavigator().gotoNavigator();
        tester.assertTableNotPresent("issuetable");
        tester.assertLinkNotPresentWithText("TST-2");
    }

    public void testAddingWatchersViaWatcherManagerReindexesWatchersCustomField()
    {
        testRemovingWatchersViaWatcherManagerReindexesWatchersCustomField();

        issueNavigation.gotoIssue("TST-2");
        tester.clickLink("manage-watchers");
        tester.setWorkingForm("startform");
        tester.setFormElement("userNames", "user1");
        tester.submit("add");

        navigation.gotoPage("issues/?jql=\"Watchers Field\" = user1");
        issueNavigatorAssertions.assertIssueNavigatorDisplaying(locator.page(), "1", "1", "1");
    }

    // VOTE-26
    public void testReporterCannotVoteOwnIssue() throws IOException
    {
        issueNavigation.gotoIssue("TST-2");
        navigation.gotoPage("ViewIssue.jspa?id=10001&vote=vote");
        tester.assertTextPresent("You cannot vote for an issue you have reported.");
    }
}
