package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.MockUser;
import junit.framework.TestCase;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

public abstract class AbstractCFTypeTestCase extends TestCase
{
    @Mock
    protected UserConverter userConverter;
    @Mock
    protected JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    protected User user;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        when(userConverter.getString((User) anyObject())).thenAnswer(
                new Answer()
                {
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return ((User) invocationOnMock.getArguments()[0]).getName();
                    }
                }
        );
        when(userConverter.getUser(anyString())).thenAnswer(
                new Answer()
                {
                    public Object answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        return new MockUser((String) invocationOnMock.getArguments()[0], (String) invocationOnMock.getArguments()[0], invocationOnMock.getArguments()[0] + "@atlassian.com");
                    }
                }
        );

        user = new MockUser("admin", "admin", "admin@atlassian.com");
    }

    @Override
    protected void tearDown() throws Exception
    {
        userConverter = null;
        super.tearDown();
    }

    protected abstract CalculatedCFType getTestCFType();

    /* TODO: Investigate whether we can fix the tests that is commented out below or remove completely after fixing VOTE-49 */
//    public void testConvertSingularObjectToString()
//    {
//        assertEquals(user.getName(), getTestCFType().getStringFromSingularObject(user));
//    }
//
//    public void testConvertStringToSingularObject()
//    {
//        assertEquals(user, getTestCFType().getSingularObjectFromString(user.getName()));
//    }
}
