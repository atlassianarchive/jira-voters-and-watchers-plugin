package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import static org.mockito.Mockito.when;

public class VoteCFTypeTestCase extends AbstractCFTypeTestCase
{
    /* TODO: Investigate whether we can fix the tests that is commented out below or remove completely after fixing VOTE-49 */

    @Mock
    private VoteManager voteManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private Issue issue;
    @Mock
    private CustomField customField;
    @Mock
    private FieldLayoutItem fieldLayoutItem;
    @Mock
    private GenericValue issueGv;
//    @Mock
//    private JiraWebResourceManager jiraWebResourceManager;

    private VoteCFType voteCFType;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(issue.getGenericValue()).thenReturn(issueGv);

//        voteCFType = new TestVoteCFType(
//                userConverter,
//                voteManager,
//                jiraAuthenticationContext
//        );
    }

    @Override
    protected void tearDown() throws Exception
    {
        voteManager = null;
        jiraAuthenticationContext = null;
        issue = null;
        customField = null;
        fieldLayoutItem = null;
        issueGv = null;
        super.tearDown();
    }

    @Override
    protected CalculatedCFType getTestCFType()
    {
        return voteCFType;
    }

    public void testVotingEnableStateAsCustomFieldValueOfIssue()
    {
//        when(voteManager.isVotingEnabled()).thenReturn(true);
//        assertEquals(true, voteCFType.getValueFromIssue(null, null));
    }

//    public void testIssueVotedIfVoteManagerSaysUserHasVotedOnIssue()
//    {
//        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
//        when(voteManager.hasVoted(user, issueGv)).thenReturn(true);
//
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(true, velocityParams.get("voted"));
//    }
//
//    public void testIssueNotVotedIfVoteManagerSaysUserHasNotVotedOnIssue()
//    {
//        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
//
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(false, velocityParams.get("voted"));
//    }
//
//    public void testIssueNotVotedIfUserIsAnonymous()
//    {
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(false, velocityParams.get("voted"));
//    }
//
//    public void testCurrentUserIsReporterIfIssueHasReporterEqualsToCurrentUser()
//    {
//        when(issue.getReporter()).thenReturn(user);
//        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
//
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(true, velocityParams.get("reporter"));
//    }
//
//    public void testCurrentUserIsNotReporterIfIssueDoesNotHaveReporter()
//    {
//        when(jiraAuthenticationContext.getLoggedInUser()).thenReturn(user);
//
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(false, velocityParams.get("reporter"));
//    }
//
//    public void testCurrentUserIsNotReporterIfHeOrSheIsAnonymous()
//    {
//        when(issue.getReporter()).thenReturn(user);
//
//        Map velocityParams = voteCFType.getVelocityParameters(issue, customField, fieldLayoutItem);
//
//        assertEquals(false, velocityParams.get("reporter"));
//    }
//
//    private class TestVoteCFType extends VoteCFType
//    {
//        public TestVoteCFType(UserConverter userConverter, VoteManager voteManager, JiraAuthenticationContext jiraAuthenticationContext)
//        {
//            super(userConverter, voteManager, jiraAuthenticationContext);
//        }
//    }
}
