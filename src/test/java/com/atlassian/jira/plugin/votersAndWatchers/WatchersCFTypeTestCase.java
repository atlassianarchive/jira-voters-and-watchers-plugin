package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.user.MockUser;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.when;

public class WatchersCFTypeTestCase extends AbstractCFTypeTestCase
{
    @Mock
    private WatcherManager watcherManager;

    @Mock
    private Issue issue;

    @Mock
    private GenericValue issueGv;

    @Mock
    private CustomField customField;

    @Mock
    private ApplicationProperties applicationProperties;


    private WatchersCFType watchersCFType;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(issue.getGenericValue()).thenReturn(issueGv);
        watchersCFType = new WatchersCFType(userConverter, watcherManager, jiraAuthenticationContext, applicationProperties);

        when(applicationProperties.getOption("jira.option.watching")).thenReturn(true);

        when(customField.getId()).thenReturn("10000");
    }

    @Override
    protected void tearDown() throws Exception
    {
        watcherManager = null;
        issue = null;
        issueGv = null;
        customField = null;
        super.tearDown();
    }

    protected CalculatedCFType getTestCFType()
    {
        return watchersCFType;
    }

    public void testSortedWatchersAsValue()
    {
        User user = new MockUser("admin", "admin", "admin@atlassian.com");
        User watcher2 = new MockUser("watcher2", "watcher2", "watcher2@atlassian.com");

        when(watcherManager.getCurrentWatchList(same(issue), (Locale) anyObject())).thenReturn(Arrays.asList(watcher2, user));

        assertEquals(Arrays.asList(user, watcher2), watchersCFType.getValueFromIssue(null, issue));
    }

    public void testWatcherNamesIndexed()
    {
        @SuppressWarnings("unchecked")
        List<FieldIndexer> fieldIndexers = watchersCFType.getRelatedIndexers(customField);

        assertEquals(1, fieldIndexers.size());

        FieldIndexer fieldIndexer = fieldIndexers.get(0);

        assertEquals(customField.getId(), fieldIndexer.getId());

        when(customField.getValue(issue)).thenReturn(Arrays.asList(user));

        Document document = new Document();

        fieldIndexer.addIndex(document, issue);
        Field field = document.getField(customField.getId());

        assertEquals(userConverter.getString(user), field.stringValue());
        assertTrue(field.isStored());
        assertFalse(field.isTokenized());
    }
}
