package com.atlassian.jira.plugin.votersAndWatchers;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.impl.CalculatedCFType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.index.indexers.FieldIndexer;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.user.MockUser;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.mockito.Mock;
import org.ofbiz.core.entity.GenericValue;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.same;
import static org.mockito.Mockito.when;

public class VotersCFTypeTestCase extends AbstractCFTypeTestCase
{
    @Mock
    private VoteManager voteManager;

    @Mock
    private Issue issue;

    @Mock
    private GenericValue issueGv;

    @Mock
    private CustomField customField;

    private VotersCFType votersCFType;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        when(issue.getGenericValue()).thenReturn(issueGv);
        votersCFType = new VotersCFType(userConverter, voteManager, jiraAuthenticationContext);

        when(customField.getId()).thenReturn("10000");
    }

    @Override
    protected void tearDown() throws Exception
    {
        voteManager = null;
        issue = null;
        issueGv = null;
        customField = null;
        super.tearDown();
    }

    protected CalculatedCFType getTestCFType()
    {
        return votersCFType;
    }

    public void testSortedVotersAsValueIfVotingIsEnabled()
    {
        User user = new MockUser("admin", "admin", "admin@atlassian.com");
        User voter2 = new MockUser("voter2", "voter2", "voter2@atlassian.com");

        when(voteManager.isVotingEnabled()).thenReturn(true);
        when(voteManager.getVoters(same(issue), (Locale) anyObject())).thenReturn(Arrays.asList(voter2, user));

        assertEquals(Arrays.asList(user, voter2), votersCFType.getValueFromIssue(null, issue));
    }

    public void testStringAsValueIfVotingIsNotEnabled()
    {

        assertTrue(votersCFType.getValueFromIssue(null, issue) instanceof String);
    }

    public void testVoterNamesIndexed()
    {
        @SuppressWarnings("unchecked")
        List<FieldIndexer> fieldIndexers = votersCFType.getRelatedIndexers(customField);

        when(voteManager.isVotingEnabled()).thenReturn(true);

        assertEquals(1, fieldIndexers.size());

        FieldIndexer fieldIndexer = fieldIndexers.get(0);

        assertEquals(customField.getId(), fieldIndexer.getId());

        when(customField.getValue(issue)).thenReturn(Arrays.asList(user));

        Document document = new Document();

        fieldIndexer.addIndex(document, issue);
        Field field = document.getField(customField.getId());

        assertEquals(userConverter.getString(user), field.stringValue());
        assertTrue(field.isStored());
        assertFalse(field.isTokenized());
    }
}
